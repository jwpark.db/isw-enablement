[Home](../README.md)


# Table of Contents
- [Pipeline](#pipeline)
- [Deployment](#deployment)

---

## Pipeline

![pipeline](images/pipeline-trigger-2.png)

- Type은 helm chart로 release까지만 할 것인지, 아니면 deploy가지 해야 하는지 설정
- Deploy 설정 시  target namespace도 설정 해야함.

### pipeline 구성
![pipeline tasks](images/pipeline-tasks.png)

- Predefined된 pipeline 사용
- 조직에 일괄적인 pipeline 적용으로 표준화 할 수 있음
- 개발쪽에서 추가적인 configuration 없이 deployment target에 맞게 ISW가 설정함
- 추가 custom pipeline 사용 가능

## Release
- Release로 Pipeline을 만들 경우, 최종 단계는 Deploy 앞단인 helm chart release
- 추후 관리자에 의해 결정되어 배포 될 경우나, 정책적으로 배포를 관리 할 경우 사용 가능

### Composition Project
- Release로 만들어진 Project들을 모아서 하나의 서비스처럼 배포 하기 위해 사용
- 조직 구성도나, 관리 측면에서 사용 가능 하며 마이크로 서비스이지만 서로간 Dependency가 있을 경우 묶어 배포

## Deployment
![solutionhub](images/solution%20hub%20position.png)
![solutionenvoy](images/solution%20envoy%20position.png)

- solution hub -> solution Envoy 접근하여 deployment 확인
https://education-dev.apps.openshift-01.knowis.cloud/solutions.html